/*
  LoRa Duplex communication

  Sends a message every half second, and polls continually
  for new incoming messages. Implements a one-byte addressing scheme,
  with 0xFF as the broadcast address.

  Uses readString() from Stream class to read payload. The Stream class'
  timeout may affect other functuons, like the radio's callback. For an

  created 28 April 2017
  by Tom Igoe
*/
#include <SPI.h>              // include libraries
#include <LoRa.h>

#define MAX_PACKET_SIZE 300

const int csPin = 7;          // LoRa radio chip select
const int resetPin = 6;       // LoRa radio reset
const int irqPin = 1;         // change for your board; must be a hardware interrupt pin

void setup() {
  Serial.begin(9600);                   // initialize serial
  while (!Serial);

  // override the default CS, reset, and IRQ pins (optional)
  LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin

  if (!LoRa.begin(866E6)) {             // initialize ratio at 915 MHz
    while (true);                       // if failed, do nothing
  }

  LoRa.onReceive(onLoRaReceive);
  LoRa.receive();
}

void loop() {}

/**
* Event that is fired when data is being recieved via serial USB interface.
*/
void serialEvent() {
  byte buffer[MAX_PACKET_SIZE]={};
  LoRa.beginPacket();                   // start packet
  while (Serial.available()) {
    size_t dataSize=Serial.readBytes(buffer,MAX_PACKET_SIZE);
    LoRa.write(buffer,dataSize);
  }
  LoRa.endPacket();                     // finish packet and send it
  LoRa.receive();//Listen to the data again
}


void onLoRaReceive(int packetSize) {
  if (packetSize == 0) {
    return;          // if there's no packet, return
  }

  String incoming="";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }

  if (packetSize != incoming.length()) {   // check length for error
    return;                             // skip rest of function
  }

  if(Serial){
    Serial.print(incoming);
  }
}
